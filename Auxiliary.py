from main import *
import matplotlib.pyplot as plt
import scipy as sp
import numpy as np

def init(ref_path, meas_path, categoryNbr):
    r"""
    Parameters
    ----------

    Returns
    ----------
    J :
        Matrix for the linearization of the forward problem, :math:`J : R^{1602} -> R^{2356}`
    deltaU :
        RHS of the linear forward problem (i.e solve :math:`J @ x = deltaU \in R^{2356}` for deltaU)
    sigma0 :
        linearization point, i.e. :math:`x \in R^{1602}` with constant :math:`x = 0`
    smprior.L :
        Prior matrix  `L_{pr}` in the regularization Term, i.e. in the FEM-space (1602 dim)
    solver.Ln :
        weighting matrix `L_{Delta e}` for the data discrepancy term, i.e. in measurement space (2356 dim)
    H : 
        Connectivity list of given FEm triangulation
    g :
        List of Points of the Triangulation
    """
    Nel = 32  # number of electrodes
    z = (1e-6) * np.ones((Nel, 1))  # contact impedances
    mat_dict = sp.io.loadmat(ref_path) #load the reference data
    Injref = mat_dict["Injref"] #current injections
    Uelref = mat_dict["Uelref"] #measured voltages from water chamber
    Mpat = mat_dict["Mpat"] #voltage measurement pattern
    vincl = np.ones(((Nel - 1),76), dtype=bool) #which measurements to include in the inversion
    rmind = np.arange(0,2 * (categoryNbr - 1),1) #electrodes whose data is removed

    #remove measurements according to the difficulty level
    for ii in range(0,75):
        for jj in rmind:
            if Injref[jj,ii]:
                vincl[:,ii] = 0
            vincl[jj,:] = 0

    # load premade finite element mesh (made using Gmsh, exported to Matlab and saved into a .mat file)
    mat_dict_mesh = sp.io.loadmat('Mesh_sparse.mat')
    g = mat_dict_mesh['g'] #node coordinates
    H = mat_dict_mesh['H'] #indices of nodes making up the triangular elements
    elfaces = mat_dict_mesh['elfaces'][0].tolist() #indices of nodes making up the boundary electrodes

    #Element structure
    ElementT = mat_dict_mesh['Element']['Topology'].tolist()
    for k in range(len(ElementT)):
        ElementT[k] = ElementT[k][0].flatten()
    ElementE = mat_dict_mesh['ElementE'].tolist() #marks elements which are next to boundary electrodes
    for k in range(len(ElementE)):
        if len(ElementE[k][0]) > 0:
            ElementE[k] = [ElementE[k][0][0][0], ElementE[k][0][0][1:len(ElementE[k][0][0])]]
        else:
            ElementE[k] = []

    #Node structure
    NodeC = mat_dict_mesh['Node']['Coordinate']
    NodeE = mat_dict_mesh['Node']['ElementConnection'] #marks which elements a node belongs to
    nodes = [KTCMeshing.NODE(coord[0].flatten(), []) for coord in NodeC]
    for k in range(NodeC.shape[0]):
        nodes[k].ElementConnection = NodeE[k][0].flatten()
    elements = [KTCMeshing.ELEMENT(ind, []) for ind in ElementT]
    for k in range(len(ElementT)):
        elements[k].Electrode = ElementE[k]

    #2nd order mesh data
    H2 = mat_dict_mesh['H2']
    g2 = mat_dict_mesh['g2']
    elfaces2 = mat_dict_mesh['elfaces2'][0].tolist()
    ElementT2 = mat_dict_mesh['Element2']['Topology']
    ElementT2 = ElementT2.tolist()
    for k in range(len(ElementT2)):
        ElementT2[k] = ElementT2[k][0].flatten()
    ElementE2 = mat_dict_mesh['Element2E']
    ElementE2 = ElementE2.tolist()
    for k in range(len(ElementE2)):
        if len(ElementE2[k][0]) > 0:
            ElementE2[k] = [ElementE2[k][0][0][0], ElementE2[k][0][0][1:len(ElementE2[k][0][0])]]
        else:
            ElementE2[k] = []

    NodeC2 = mat_dict_mesh['Node2']['Coordinate']  # ok
    NodeE2 = mat_dict_mesh['Node2']['ElementConnection']  # ok
    nodes2 = [KTCMeshing.NODE(coord[0].flatten(), []) for coord in NodeC2]
    for k in range(NodeC2.shape[0]):
        nodes2[k].ElementConnection = NodeE2[k][0].flatten()
    elements2 = [KTCMeshing.ELEMENT(ind, []) for ind in ElementT2]
    for k in range(len(ElementT2)):
        elements2[k].Electrode = ElementE2[k]

    Mesh = KTCMeshing.Mesh(H,g,elfaces,nodes,elements)
    Mesh2 = KTCMeshing.Mesh(H2,g2,elfaces2,nodes2,elements2)

    # print(f'Nodes in inversion 1st order mesh: {len(Mesh.g)}')

    sigma0 = np.ones((len(Mesh.g), 1)) #linearization point

    corrlength = 1 * 0.115 #used in the prior
    var_sigma = 0.05 ** 2 #prior variance
    mean_sigma = sigma0
    smprior = KTCRegularization.SMPrior(Mesh.g, corrlength, var_sigma, mean_sigma)

    # set up the forward solver for inversion
    solver = KTCFwd.EITFEM(Mesh2, Injref, Mpat, vincl)

    vincl = vincl.T.flatten()

    # set up the noise model for inversion
    noise_std1 = 0.05;  # standard deviation for first noise component (relative to each voltage measurement)
    noise_std2 = 0.01;  # standard deviation for second noise component (relative to the largest voltage measurement)
    solver.SetInvGamma(noise_std1, noise_std2, Uelref)

    # Get a list of .mat files in the input folder
    #mat_files = glob.glob(inputFolder + '/data*.mat')
    """for objectno in range (0,len(mat_files)): #compute the reconstruction for each input file"""
    #objectno = 0 
    """fixed No 0 for testing purposes"""
    mat_dict2 = sp.io.loadmat(meas_path)
    Inj = np.expand_dims(mat_dict2["Inj"][:, 1], axis=1)
    Uel = np.expand_dims(mat_dict2["Uel"][:, 1], axis=1)
    Mpat = np.expand_dims(mat_dict2["Mpat"][:, 1], axis=1)
    deltaU = Uel - Uelref

    Usim = solver.SolveForward(sigma0, z) #forward solution at the linearization point
    J = solver.Jacobian(sigma0, z)
    mask = np.array(vincl, bool)

    return J, deltaU, sigma0, smprior.L, solver.Ln, H, g, Mesh, solver, mask, vincl


def create_Nabla(H, g, weighted = True):
    Nabla = np.zeros((3 * np.shape(H)[0], np.shape(g)[0]))
    for i in range(np.shape(H)[0]):
        if weighted:
            dist12 = np.linalg.norm(g[H[i,0], :]-g[H[i,1],:])
            dist13 = np.linalg.norm(g[H[i,0], :]-g[H[i,2], :])
            dist23 = np.linalg.norm(g[H[i,1],:]-g[H[i,2],:])
        else:
            dist12 = 1
            dist13 = 1
            dist23 = 1
        #edge from point1 to point2
        Nabla[3 * i, H[i,0]]= 1/dist12
        Nabla[3 * i, H[i,1]]= -1/dist12
        #edge from point1 to point3
        Nabla[3 * i + 1, H[i,0]]= 1/dist13
        Nabla[3 * i + 1, H[i,2]]= -1/dist13
        #edge from point2 to point3
        Nabla[3 * i + 2, H[i,1]]= 1/dist23
        Nabla[3 * i + 2, H[i,2]]= -1/dist23
    return Nabla

def mbfunc(q, ub):
    out = np.inf * np.ones(q.shape)
    for i in range(len(ub)-1):
        ind = np.logical_and(ub[i] <= q, q < ub[i+1])
        np.putmask(out, ind, 0.5 * ((ub[i] + ub[i+1]) * q - ub[i] * ub[i+1]))
        #out[ind] = 0.5 * ((ub[i] + ub[i+1]) * q[ind] - ub[i] - ub[i+1])
    return np.mean(out)


def mbprox(q, ub, gamma, alpha):
    #q : input argument
    # ub: vector of admissible states
    # gamma, alpha: parameters of the algorithms

    d = np.size(ub)
    ag = alpha * gamma / 2
    w = 0. * q
    #print(w)
    w[q <= (1 + ag) * ub[0] + ag * ub[1]] = ub[0]
    for i in range(1, d-1):
        ind1 = np.logical_and(((1 + ag)*ub[i-1] + ag * ub[i]) < q, q <(ag * ub[i-1] + (1 + ag) * ub[i]))
        #print(np.shape(ind1), np.shape(w), np.shape(ind1.flat))
        np.putmask(w, ind1.flat, q[ind1] - ag * (ub[i-1] + ub[i]))
        #w[ind1]= q[ind1] - ag * (ub[i-1] + ub[i])

        ind2 = np.logical_and((ag * ub[i-1] + (1 + ag) * ub[i]) <= q, q <= ((1 + ag)*ub[i] + ag * ub[i + 1]))
        np.putmask(w, ind2.flat, ub[i])
        #w[ind2] = ub[i]
        
    
    ind3 = np.logical_and(((1 + ag)*ub[d-2] + ag * ub[d-1]) < q, q <(ag * ub[d-2] + (1 + ag) * ub[d-1]))
    np.putmask(w, ind3.flat, q[ind3] - ag * (ub[d-2] + ub[d-1]))
    #w[ind3] = q[ind3] - ag * (ub[d-2] + ub[d-1])
    w[q >= (1 + ag)*ub[d-1] + ag*ub[d-2]] = ub[d-1]
    return w

def L2_multibang_direct(deltaU, Ln, L_pr, J, alpha = 1e-1, vec_MB = np.linspace(-50, 50, 15),):
    #!!!!!!!!!! F und G vertauscht
    m, n = Ln.shape
    u0 = np.zeros((1602,1))
    v = u0.copy()
    p0 = np.zeros((n, 1))
    sigma, tau = .9 * np.linalg.norm(J)**-1, .9 * np.linalg.norm(J)**-1
    u = u0
    p = p0
    y = deltaU
    maxit = 500
    it = 0
    #eps = 1e-6
    #diff = np.Inf
    invG = np.linalg.pinv(sigma * Ln.T @ Ln + np.eye(n))
    #invF = np.linalg.pinv(tau * L_pr.T @ L_pr + np.eye(1602))
    K = J
    #def proxF(sigma, x):
    #    return invF @ x
    def proxG(sigma, x):
        #print(np.shape(sigma * solver.Ln.T @ solver.Ln), np.shape(y), np.shape(sigma * solver.Ln.T @ solver.Ln @ y), np.shape(x), (np.shape(x + y)))
        return invG @ (sigma * Ln.T @ Ln @ y + x)
        #return (sigma * y + x)/(1 + sigma)
    proxF = lambda sigma, x: mbprox(x, vec_MB, sigma, alpha)
    def proxGstar(sigma, x):
        return x - sigma * proxG(1/sigma, x/sigma)
    while it < maxit:# and diff > eps * np.linalg.norm(u) * np.linalg.norm(K):
        #print("1", np.shape(u), np.shape(v), np.shape(p))
        pnew = proxGstar(sigma, p + sigma * K @ v)
        #print("2", np.shape(u), np.shape(v), np.shape(pnew))
        unew = proxF(tau, u - tau * K.T @ pnew)
        #print("3", np.shape(u), np.shape(v), np.shape(p))
        v = 2 * unew - u
        #print("4", np.shape(u), np.shape(v), np.shape(p))
        #diff = np.linalg.norm(v - u)
        u, p = unew, pnew
        #print(np.shape(u))
        it += 1
        #if it%100 == 0 : print(it)
    #print("Done! L2_multibang_direct")
    return u

def L2_TV_multibang_direct(deltaU, Ln, L_pr, J, Nabla, alpha1, alpha2, vec_MB = np.linspace(-50, 50, 15)):
    u = np.zeros((1602,1))
    v = u.copy()
    p1 = np.zeros((2356, 1))
    p2 = np.zeros((9219, 1))
    it = 0
    maxit = 500
    sigma, tau = .9 * np.linalg.norm(np.vstack((J, Nabla)))**-1, .9 * np.linalg.norm(np.vstack((J, Nabla)))**-1
    inner = Ln.T @Ln
    #print(np.shape(inner.toarray()))
    Inv_inner = np.linalg.pinv(inner.toarray())
    #print(np.shape(Inv_inner))
    InvF1star = np.linalg.pinv(np.eye(2356) + sigma * Inv_inner)
    def proxF1star(sigma, z):
        out = InvF1star @ (z - sigma * deltaU)
        return out
    
    def proxF2star(sigma, z):
        return alpha1 * z / np.max([alpha1, np.linalg.norm(z, 1)])
    
    proxG = lambda sigma, x: mbprox(x, vec_MB, sigma, alpha2)

    while it < maxit:
        p1 = proxF1star(sigma, p1 + sigma * J @ v)
        p2 = proxF2star(sigma, p2 + sigma * Nabla @v)
        unew = proxG(tau, u - tau * J.T @ p1 - tau * Nabla.T @p2)
        v = 2*unew - u
        u = unew
        it += 1
        #if it%100 == 0 : print(it)
    #print("Done! L2_TV_multibang_direct")
    return u

def L2_multibang_segmentation(reco, alpha, vec_MB = np.linspace(-2.5, 2.5, 3)):
    u = reco
    p = np.zeros((1602, 1))
    v = u.copy()
    it = 0
    sigma, tau = .09, .09
    def proxFstar(sigma, z):
        return (z - sigma * reco)/(1 + sigma)
    proxG = lambda sigma, x: mbprox(x, vec_MB, sigma, alpha)
    #proxG = lambda sigma, x: mbprox(x, np.linspace(-5, 5, 5), sigma, alpha)

    maxit = 2000
    while it < maxit:
        p = proxFstar(sigma, p + sigma * v)
        unew = proxG(tau, u - tau * p)
        v = 2 * unew - u
        u = unew
        it += 1
        #if it%100 == 0 : print(it)
    #print("Done! L2_multibang_segmentation")
    return u

def L2_TV_multibang_segmentation(reco, Nabla, alpha1, alpha2, vec_MB = np.linspace(-50, 50, 15)):
    u = reco
    v = u.copy()
    p1 = np.zeros((1602, 1))
    p2 = np.zeros((9219, 1))
    sigma, tau = .9 * np.linalg.norm(np.vstack((np.eye(1602), Nabla)))**-1, .9 * np.linalg.norm(np.vstack((np.eye(1602), Nabla)))**-1
    def proxF1star(sigma, z):
        return (z - sigma * reco)/(1 + sigma)

    def proxF2star(sigma, z):
        return alpha1 * z / np.max([alpha1, np.linalg.norm(z, 1)])

    proxG = lambda sigma, x: mbprox(x, vec_MB, sigma, alpha2)

    it = 0
    maxit = 2000
    while it < maxit:
        p1 = proxF1star(sigma, p1 + sigma * v)
        p2 = proxF2star(sigma, p2 + sigma * Nabla @ v)
        unew = proxG(tau, u - p1 - Nabla.T @p2)
        v = 2 * unew - u
        u = unew
        it += 1
        #if it%100 == 0 : print(it)
    #print("Done! L2_TV_multibang_segmentation")
    return u







def L2_multibang_direct_balancing(deltaU, Ln, L_pr, J, alpha0 = 1e-1, vec_MB = np.linspace(-50, 50, 15),):
    #!!!!!!!!!! F und G vertauscht
    u0 = np.zeros((1602,1))
    v = u0.copy()
    p0 = np.zeros((2356, 1))
    sigma, tau = .9 * np.linalg.norm(J)**-1, .9 * np.linalg.norm(J)**-1
    u = u0
    p = p0
    y = deltaU
    maxit = 500
    it = 0
    #eps = 1e-6
    #diff = np.Inf
    invG = np.linalg.pinv(sigma * Ln.T @ Ln + np.eye(2356))
    #invF = np.linalg.pinv(tau * L_pr.T @ L_pr + np.eye(1602))
    K = J
    alpha = alpha0
    #def proxF(sigma, x):
    #    return invF @ x
    def proxG(sigma, x):
        #print(np.shape(sigma * solver.Ln.T @ solver.Ln), np.shape(y), np.shape(sigma * solver.Ln.T @ solver.Ln @ y), np.shape(x), (np.shape(x + y)))
        return invG @ (sigma * Ln.T @ Ln @ y + x)
        #return (sigma * y + x)/(1 + sigma)
    proxF = lambda sigma, x, alpha: mbprox(x, vec_MB, sigma, alpha)
    def proxGstar(sigma, x):
        return x - sigma * proxG(1/sigma, x/sigma)
    while it < maxit:# and diff > eps * np.linalg.norm(u) * np.linalg.norm(K):
        #print("1", np.shape(u), np.shape(v), np.shape(p))
        pnew = proxGstar(sigma, p + sigma * K @ v)
        #print("2", np.shape(u), np.shape(v), np.shape(pnew))
        unew = proxF(tau, u - tau * K.T @ pnew, alpha)
        #print("3", np.shape(u), np.shape(v), np.shape(p))
        v = 2 * unew - u
        #print("4", np.shape(u), np.shape(v), np.shape(p))
        #diff = np.linalg.norm(v - u)
        u, p = unew, pnew
        print("mbfunc = ", mbfunc(u, vec_MB))
        print("norm = ", np.linalg.norm(J@u - y))
        """alpha = 1/ (np.linalg.norm(J@u - y) / mbfunc(u, vec_MB))
        if alpha == np.inf:
            print("Inf")
            alpha = 3
        elif alpha == 0:
            print("alpha = 0")
            alpha = 1e-3"""
        """if np.linalg.norm(J@u - y) > mbfunc(u, vec_MB):
            alpha /= 5
        else:
            alpha *= 5"""
        if it >= 350:
            alpha = 1/ (np.linalg.norm(J@u - y) / mbfunc(u, vec_MB))
        print(alpha)
        #print(np.shape(u))
        it += 1
        #if it%100 == 0 : print(it)
    #print("Done! L2_multibang_direct")
    return u


#
#
#



def L2_multibang_segmentation_balancing(reco, alpha0, vec_MB = np.linspace(-2.5, 2.5, 3)):
    u = reco
    p = np.zeros((1602, 1))
    v = u.copy()
    it = 0
    sigma, tau = .09, .09
    alpha = alpha0
    def proxFstar(sigma, z):
        return (z - sigma * reco)/(1 + sigma)
    proxG = lambda sigma, x, alpha: mbprox(x, vec_MB, sigma, alpha)
    #proxG = lambda sigma, x: mbprox(x, np.linspace(-5, 5, 5), sigma, alpha)

    maxit = 2000
    while it < maxit:
        p = proxFstar(sigma, p + sigma * v)
        unew = proxG(tau, u - tau * p, alpha)
        v = 2 * unew - u
        if np.linalg.norm(u - reco) > mbfunc(u, vec_MB):
            alpha /= 5
        else:
            alpha *= 5
        print("norm = ", np.linalg.norm(u - reco))
        print("MB = ", mbfunc(u, vec_MB))
        print(alpha)
        u = unew
        it += 1
        #if it%100 == 0 : print(it)
    #print("Done! L2_multibang_segmentation")
    return u

def L2_TV_multibang_segmentation_balancing(reco, Nabla, alpha10, alpha20, vec_MB = np.linspace(-50, 50, 15)):
    u = reco
    v = u.copy()
    p1 = np.zeros((1602, 1))
    p2 = np.zeros((9219, 1))
    sigma, tau = .9 * np.linalg.norm(np.vstack((np.eye(1602), Nabla)))**-1, .9 * np.linalg.norm(np.vstack((np.eye(1602), Nabla)))**-1
    alpha1 = alpha10
    alpha2 = alpha20
    def proxF1star(sigma, z):
        return (z - sigma * reco)/(1 + sigma)

    def proxF2star(sigma, z, alpha1):
        return alpha1 * z / np.max([alpha1, np.linalg.norm(z, 1)])

    proxG = lambda sigma, x, alpha2: mbprox(x, vec_MB, sigma, alpha2)

    it = 0
    maxit = 2000
    while it < maxit:
        p1 = proxF1star(sigma, p1 + sigma * v)
        p2 = proxF2star(sigma, p2 + sigma * Nabla @ v, alpha1)
        unew = proxG(tau, u - p1 - Nabla.T @p2, alpha2)
        if it > 2950:
            if np.linalg.norm(u - reco) > mbfunc(u, vec_MB):
                alpha2 /= 5
            else:
                alpha2 *= 5
            if alpha1 > 1e5:
                pass
            elif np.linalg.norm(u - reco) > np.linalg.norm(Nabla @ u):
                alpha1 /= 1.25
            else:
                alpha1 *= 1.25
        print(alpha1, alpha2)
        v = 2 * unew - u
        u = unew
        it += 1
        print(it)
    print("Done! L2_TV_multibang_segmentation")
    return u
